/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoParte;

/**
 *
 * @author kevin
 */
@Named(value = "frmTipoParte")
@ViewScoped
public class TipoParteBean extends BackingBean<TipoParte> implements Serializable {

    @EJB
    TipoParteFacadeLocal tipoPartefacade_;
    TipoParte tipoParte_;

    private EstadosCRUD estado;

    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        tipoParte_ = new TipoParte();
        inicializar();
    }
    
    /*
    sobrecarga de metodos
    */

    
    @Override
    protected TipoParte getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (TipoParte item : (List<TipoParte>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdTipoParte().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    

    @Override
    protected Object getKey(TipoParte entity) {
        return entity.getIdTipoParte();
    }

    @Override
    protected MetodosGenericos<TipoParte> getFacadeLocal() {
        return tipoPartefacade_;
    }

    @Override
    protected TipoParte getEntity() {
        return tipoParte_;
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoParte_ = (TipoParte) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoParte_ = new TipoParte();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    
    
    
    
    /*
    getters y setters !!
    */
    
    
    public EstadosCRUD getEstado() {
        return estado;
    }

    public TipoParte getTipoParte_() {
        return tipoParte_;
    }

    public void setTipoParte_(TipoParte tipoParte_) {
        this.tipoParte_ = tipoParte_;
    }

    public List<TipoParte> getLista() {
        return lista;
    }

    public void setLista(List<TipoParte> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoParte> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoParte> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
}
