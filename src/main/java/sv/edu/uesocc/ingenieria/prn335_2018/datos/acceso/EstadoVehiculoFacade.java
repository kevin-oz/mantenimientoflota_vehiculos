/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoVehiculo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoVehiculo_;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoVehiculo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Vehiculo;

/**
 *
 * @author kevin
 */
@Stateless
public class EstadoVehiculoFacade extends AbstractFacade<EstadoVehiculo> implements EstadoVehiculoFacadeLocal {

    @PersistenceContext(unitName = "PU_flota")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstadoVehiculoFacade() {
        super(EstadoVehiculo.class);
    }
    
 
}
