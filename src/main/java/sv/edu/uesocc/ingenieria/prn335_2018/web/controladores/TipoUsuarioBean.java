/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoUsuarioFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoUsuario;

/**
 *
 * @author kevin
 */
@Named(value = "frmTipoUsuario")
@ViewScoped
public class TipoUsuarioBean extends BackingBean<TipoUsuario> implements Serializable {

    @EJB
    TipoUsuarioFacadeLocal tipoUsuarioFacade_;
    TipoUsuario tipoUsuario_;
    private EstadosCRUD estado;

    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        tipoUsuario_ = new TipoUsuario();
        inicializar();
    }

    /*
    metodos sobreCargados
    */
    
    
    @Override
    protected TipoUsuario getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (TipoUsuario item : (List<TipoUsuario>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdTipoUsuario().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    

    @Override
    protected Object getKey(TipoUsuario entity) {
        return entity.getIdTipoUsuario();
    }

    @Override
    protected MetodosGenericos<TipoUsuario> getFacadeLocal() {
        return tipoUsuarioFacade_;
    }

    @Override
    protected TipoUsuario getEntity() {
        return tipoUsuario_;
    }

      @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();       
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }
    
   
    
     /**
     * 
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoUsuario_ = (TipoUsuario) event.getObject();
    }
    
    
     public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoUsuario_ = new TipoUsuario();
    }
    
   
    public void btnNuevoHandler(){
        estado=EstadosCRUD.NUEVO;
    }
    
    
  /*
    Getters y Setters
    */  
    
  
    public EstadosCRUD getEstado() {
        return estado;
    }

    public TipoUsuario getTipoUsuario_() {
        return tipoUsuario_;
    }

    public void setTipoUsuario_(TipoUsuario tipoUsuario_) {
        this.tipoUsuario_ = tipoUsuario_;
    }

    public List<TipoUsuario> getLista() {
        return lista;
    }

    public void setLista(List<TipoUsuario> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoUsuario> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoUsuario> lazyModel) {
        this.lazyModel = lazyModel;
    }

    
}
