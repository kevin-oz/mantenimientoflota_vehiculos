/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoVehiculoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoVehiculo;

/**
 *
 * @author kevin
 */
@Named(value = "frmTipoVehiculo")
@ViewScoped
public class TipoVehiculoBean extends BackingBean<TipoVehiculo> implements Serializable {

    @EJB
    TipoVehiculoFacadeLocal tipoVehiculofacade_;
    TipoVehiculo tipoVehiculo_;
    private EstadosCRUD estado;

    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        tipoVehiculo_= new TipoVehiculo();
        inicializar();
    }
    
    /*
    sobrecarga de metodos
    */

    @Override
    protected TipoVehiculo getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (TipoVehiculo item : (List<TipoVehiculo>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdTipoVehiculo().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    
    @Override
    protected Object getKey(TipoVehiculo entity) {
        return entity.getIdTipoVehiculo();
    }

    @Override
    protected MetodosGenericos<TipoVehiculo> getFacadeLocal() {
        return tipoVehiculofacade_;
    }

    @Override
    protected TipoVehiculo getEntity() {
        return tipoVehiculo_;
    }

        @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoVehiculo_ = (TipoVehiculo) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoVehiculo_ = new TipoVehiculo();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    
    /*
    getters y setters !!
    */
    public EstadosCRUD getEstado() {
        return estado;
    }
    
    public TipoVehiculo getTipoVehiculo_() {
        return tipoVehiculo_;
    }

    public void setTipoVehiculo_(TipoVehiculo tipoVehiculo_) {
        this.tipoVehiculo_ = tipoVehiculo_;
    }

    public List<TipoVehiculo> getLista() {
        return lista;
    }

    public void setLista(List<TipoVehiculo> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoVehiculo> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoVehiculo> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
    
    
}
