/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ViajeFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Viaje;

/**
 *
 * @author kevin
 */
@Named(value = "frmViaje")
@ViewScoped
public class ViajeBean extends BackingBean<Viaje> implements Serializable{

    @EJB
    ViajeFacadeLocal viajeFacade_;
    Viaje viaje_;
    private EstadosCRUD estado;

  
    
    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        viaje_= new Viaje();
        inicializar();
    }
    
    @Override
    protected Viaje getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Viaje item : (List<Viaje>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdReserva().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }
        return null;
    }
    

    @Override
    protected Object getKey(Viaje entity) {
        return entity.getIdReserva();
    }

    @Override
    protected MetodosGenericos<Viaje> getFacadeLocal() {
        return viajeFacade_;
    }

    @Override
    protected Viaje getEntity() {
        return viaje_;
    }
    
     @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        btncancelarHandler();
        estado = EstadosCRUD.EDITAR;
        viaje_ = (Viaje) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        viaje_ = new Viaje();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }
    
    
    //****** Getters y Setters *****
      

    public EstadosCRUD getEstado() {
        return estado;
    }

    public Viaje getViaje_() {
        return viaje_;
    }

    public void setViaje_(Viaje viaje_) {
        this.viaje_ = viaje_;
    }

    public List<Viaje> getLista() {
        return lista;
    }

    public void setLista(List<Viaje> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Viaje> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Viaje> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
}
