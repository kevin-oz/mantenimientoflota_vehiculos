/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.RecorridoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ViajeFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Recorrido;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Viaje;

/**
 *
 * @author kevin
 */
@Named(value = "frmRecorrido")
@ViewScoped
public class RecorridoBean extends BackingBean<Recorrido> implements Serializable{    

    
    @EJB
    RecorridoFacadeLocal recorridoFacade_;
    Recorrido recorrido_;
    private EstadosCRUD estado;
    
    @EJB
    ViajeFacadeLocal viaje_facade;
    private List<Viaje> listaViaje;
    
    
    public List<Viaje> listarViajes(){
        if(viaje_facade.findAll()!=null){
            return listaViaje=viaje_facade.findAll();
        }else{
            return listaViaje=Collections.EMPTY_LIST;
        }
    }

    public List<Viaje> getListaViaje() {
        return listaViaje;
    }
    
    
    @PostConstruct
    public void init(){
        estado= EstadosCRUD.NONE;
        recorrido_= new Recorrido();
        listarViajes();
        inicializar();
    }
   
   //******* Sobrecarga de Metodos ****** 
    
//    @Override
//    protected Integer getRowD(Recorrido object) {
//        return object.getIdRecorrido().intValue();
//    }

    
    @Override
    protected Recorrido getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Recorrido item : (List<Recorrido>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdRecorrido().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    @Override
    protected Object getKey(Recorrido entity) {
        return entity.getIdRecorrido();
    }

    @Override
    protected MetodosGenericos<Recorrido> getFacadeLocal() {
        return recorridoFacade_;
    }

    @Override
    protected Recorrido getEntity() {
        return recorrido_;
    }
    
     @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        recorrido_ = (Recorrido) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        recorrido_ = new Recorrido();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    //************Getters y Setters ******

    public EstadosCRUD getEstado() {
        return estado;
    }

    
    public Recorrido getRecorrido_() {
        return recorrido_;
    }

    public void setRecorrido_(Recorrido recorrido_) {
        this.recorrido_ = recorrido_;
    }

    public List<Recorrido> getLista() {
        return lista;
    }

    public void setLista(List<Recorrido> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Recorrido> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Recorrido> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
}
