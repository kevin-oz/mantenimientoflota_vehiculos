/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ModeloFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ModeloParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.ModeloParte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Parte;

/**
 *
 * @author kevin
 */
@Named(value = "frmModeloParte")
@ViewScoped
public class ModeloParteBean extends BackingBean<ModeloParte> implements Serializable {

    @EJB
    ModeloParteFacadeLocal modeloParteFacade_;
    ModeloParte modeloParte_;
    private EstadosCRUD estado;
    
    @EJB
    ModeloFacadeLocal modelofacade;
    private List<Modelo> listaModelo;
    
    @EJB
    ParteFacadeLocal parteFacade;
    private List<Parte> listaParte;
    
    public List<Parte> llenarListaParte(){
        if(parteFacade.findAll()!=null){
            return listaParte=parteFacade.findAll();
        }else{
            return listaParte=Collections.EMPTY_LIST;
        }
    }
    
     public List<Modelo> llenarListaModelo(){
        if(parteFacade.findAll()!=null){
            return listaModelo=modelofacade.findAll();
        }else{
            return listaModelo=Collections.EMPTY_LIST;
        }
    }

    public List<Modelo> getListaModelo() {
        return listaModelo;
    }

    public List<Parte> getListaParte() {
        return listaParte;
    }
    
     

    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        modeloParte_ = new ModeloParte();
        llenarListaModelo();
        llenarListaParte();
        inicializar();
    }

    
    //******* SobreCarga de Metodos ********

    
    
    @Override
    protected ModeloParte getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (ModeloParte item : (List<ModeloParte>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdModeloParte().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }

    @Override
    protected Object getKey(ModeloParte entity) {
        return entity.getIdModeloParte();
    }

    @Override
    protected MetodosGenericos<ModeloParte> getFacadeLocal() {
        return modeloParteFacade_;
    }

    @Override
    protected ModeloParte getEntity() {
        return modeloParte_;
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        modeloParte_ = (ModeloParte) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        modeloParte_ = new ModeloParte();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    //******** Getters y Setters ******

    public EstadosCRUD getEstado() {
        return estado;
    }
    
    public ModeloParte getModeloParte_() {
        return modeloParte_;
    }

    public void setModeloParte_(ModeloParte modeloParte_) {
        this.modeloParte_ = modeloParte_;
    }

    public List<ModeloParte> getLista() {
        return lista;
    }

    public void setLista(List<ModeloParte> lista) {
        this.lista = lista;
    }

    public LazyDataModel<ModeloParte> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<ModeloParte> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
}
