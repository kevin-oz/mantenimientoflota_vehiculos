/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores.coverter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.FacesConverter;
import org.omnifaces.converter.SelectItemsConverter;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Recorrido;

/**
 *
 * @author kevin Figueroa
 */
 @FacesConverter("recorridoConverter")
 public class ViajeConverter extends SelectItemsConverter {

     @Override
     public String getAsString(FacesContext context, UIComponent component, Object value) {
         Long id = (value instanceof Recorrido) ? ((Recorrido) value).getIdRecorrido(): null;
         return (id != null) ? String.valueOf(id) : null;
     }

     
 }