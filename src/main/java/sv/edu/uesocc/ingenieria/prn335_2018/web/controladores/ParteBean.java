/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.SubTipoParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Parte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.SubTipoParte;

/**
 *
 * @author kevin Figueroa
 */
@Named(value = "frmParte")
@ViewScoped
public class ParteBean extends BackingBean<Parte> implements Serializable {

    @EJB
    ParteFacadeLocal parteFacade_;
    Parte parte_;
    private EstadosCRUD estado;
    
    @EJB
    SubTipoParteFacadeLocal subtipoParte;
    private List<SubTipoParte> listaSubtipoParte;
    
    public List<SubTipoParte> llenadoLista(){
        if(subtipoParte.findAll()!=null){
            return listaSubtipoParte=subtipoParte.findAll();
        }else{
            return listaSubtipoParte=Collections.EMPTY_LIST;
        }
    }

    public List<SubTipoParte> getListaSubtipoParte() {
        return listaSubtipoParte;
    }
    
    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        parte_= new Parte();
        llenadoLista();
        inicializar();
    }
    

    //********** sobreCarga de Metodos*******
//    @Override
//    protected Integer getRowD(Parte object) {
//        return object.getIdParte();
//    }

    @Override
    protected Parte getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Parte item : (List<Parte>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdParte().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    @Override
    protected Object getKey(Parte entity) {
        return entity.getIdParte();
    }

    @Override
    protected MetodosGenericos<Parte> getFacadeLocal() {
        return parteFacade_;
    }

    @Override
    protected Parte getEntity() {
        return parte_;
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        parte_ = (Parte) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        parte_ = new Parte();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    
    //****** Getters y Setters *********
    
    public EstadosCRUD getEstado() {
        return estado;
    }

    public Parte getParte_() {
        return parte_;
    }

    public void setParte_(Parte parte_) {
        this.parte_ = parte_;
    }

    public List<Parte> getLista() {
        return lista;
    }

    public void setLista(List<Parte> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Parte> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Parte> lazyModel) {
        this.lazyModel = lazyModel;
    }

    
    
    
}
