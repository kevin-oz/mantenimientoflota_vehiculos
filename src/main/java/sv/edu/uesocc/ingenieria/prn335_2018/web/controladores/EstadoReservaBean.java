/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.EstadoReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoReserva;

/**
 *
 * @author kevin
 */
@Named(value = "frmEstadoReserva")
@ViewScoped
public class EstadoReservaBean extends BackingBean<EstadoReserva> implements Serializable {

    @EJB
    EstadoReservaFacadeLocal estadoReservaFacade_;
    EstadoReserva estadoReserva_;
    private EstadosCRUD estado;
    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        estadoReserva_= new EstadoReserva();
        inicializar();
    }

    //*** SobreCarga de metodos ********
    
    @Override
    protected Object getKey(EstadoReserva entity) {
        return entity.getIdEstadoReserva();
    }

    @Override
    protected MetodosGenericos<EstadoReserva> getFacadeLocal() {
        return estadoReservaFacade_;
    }

    @Override
    protected EstadoReserva getEntity() {
        return estadoReserva_;
    }

    
        @Override
    protected EstadoReserva getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (EstadoReserva item : (List<EstadoReserva>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdEstadoReserva().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    
    
    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        estadoReserva_ = (EstadoReserva) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        estadoReserva_ = new EstadoReserva();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    //******* Getters y Setters *********

    public EstadosCRUD getEstado() {
        return estado;
    }
    
    public EstadoReserva getEstadoReserva_() {
        return estadoReserva_;
    }

    public void setEstadoReserva_(EstadoReserva estadoReserva_) {
        this.estadoReserva_ = estadoReserva_;
    }

    public List<EstadoReserva> getLista() {
        return lista;
    }

    public void setLista(List<EstadoReserva> lista) {
        this.lista = lista;
    }

    public LazyDataModel<EstadoReserva> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<EstadoReserva> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
}
