/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MarcaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ModeloFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoVehiculoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Marca;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoVehiculo;

/**
 *
 * @author kevin
 */
@Named(value = "frmModelo")
@ViewScoped
public class ModeloBean extends BackingBean<Modelo> implements Serializable {

    @EJB
    ModeloFacadeLocal modeloFacade_;
    Modelo modelo_;
    private EstadosCRUD estado;
    
    @EJB
    MarcaFacadeLocal marca;
    private List<Marca> listaMarca;
    
    @EJB
    TipoVehiculoFacadeLocal tipoVehiculo;
    private List<TipoVehiculo> listaTipovehiculo;
    
    public List<Marca> llenarlistaMarca(){
        if (marca.findAll() != null) {
            return listaMarca = marca.findAll();
        } else {
            return listaMarca = Collections.EMPTY_LIST;
        }
    }
     public List<TipoVehiculo> llenarlistaTipoVehiculo(){
         if (marca.findAll() != null) {
            return listaTipovehiculo = tipoVehiculo.findAll();
        } else {
            return listaTipovehiculo = Collections.EMPTY_LIST;
        }
    }
    

    @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        modelo_ = new Modelo();
        llenarlistaMarca();
        llenarlistaTipoVehiculo();
        inicializar();
    }

    /*
    sobreCarga de Metodos
     */

    
    @Override
    protected Modelo getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Modelo item : (List<Modelo>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdModelo().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    @Override
    protected Object getKey(Modelo entity) {
        return entity.getIdModelo();
    }

    @Override
    protected MetodosGenericos<Modelo> getFacadeLocal() {
        return modeloFacade_;
    }

    @Override
    protected Modelo getEntity() {
        return modelo_;
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    
    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        modelo_ = new Modelo();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }
    
     public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        modelo_= (Modelo) event.getObject();
    }
  
    /*
    getters y Setters !!
     */
    public EstadosCRUD getEstado() {
        return estado;
    }

    public Modelo getModelo_() {
        return modelo_;
    }

    public void setModelo_(Modelo modelo_) {
        this.modelo_ = modelo_;
    }

    public List<Modelo> getLista() {
        return lista;
    }

    public void setLista(List<Modelo> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Modelo> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Modelo> lazyModel) {
        this.lazyModel = lazyModel;
    }

    public List<Marca> getListaMarca() {
        return listaMarca;
    }

    public List<TipoVehiculo> getListaTipovehiculo() {
        return listaTipovehiculo;
    }

    
    
}
