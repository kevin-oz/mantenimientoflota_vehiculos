/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ReservaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Reserva;

/**
 *
 * @author kevin
 */
@Named(value = "frmReserva")
@ViewScoped
public class ReservaBean extends BackingBean<Reserva> implements Serializable{

    @EJB
    ReservaFacadeLocal reservaFacade_;
    Reserva reserva_;
    private EstadosCRUD estado;
    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        reserva_= new Reserva();
        inicializar();
    }
    


    @Override
    protected Reserva getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Reserva item : (List<Reserva>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdReserva().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }


    @Override
    protected Object getKey(Reserva entity) {
        return entity.getIdReserva();
    }

    @Override
    protected MetodosGenericos<Reserva> getFacadeLocal() {
        return reservaFacade_;
    }

    @Override
    protected Reserva getEntity() {
        return reserva_;
    }
    
    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        reserva_ = (Reserva) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        reserva_ = new Reserva();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    
    //******** Getters y Setters ******

    public EstadosCRUD getEstado() {
        return estado;
    }
    
    
    public Reserva getReserva_() {
        return reserva_;
    }

    public void setReserva_(Reserva reserva_) {
        this.reserva_ = reserva_;
    }

    public List<Reserva> getLista() {
        return lista;
    }

    public void setLista(List<Reserva> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Reserva> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Reserva> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
}
