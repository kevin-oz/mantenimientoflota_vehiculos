/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.EstadoVehiculoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.EstadoVehiculo;

/**
 *
 * @author kevin
 */
@Named(value = "frmEstadoVehiculo")
@ViewScoped
public class EstadoVehiculoBean extends BackingBean<EstadoVehiculo> implements Serializable{

    @EJB
    EstadoVehiculoFacadeLocal estadoVehiculoFacade_;
    EstadoVehiculo estadoVehiculo_;
    private EstadosCRUD estado;
    
    @PostConstruct
    public void init(){
        estado=EstadosCRUD.NONE;
        estadoVehiculo_= new EstadoVehiculo();
        inicializar();
    }
    
    
    //********* sobreCarga de Metodos ********
    
    
    @Override
    protected EstadoVehiculo getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (EstadoVehiculo item : (List<EstadoVehiculo>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdEstadoVehiculo().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    
    @Override
    protected Object getKey(EstadoVehiculo entity) {
        return entity.getIdEstadoVehiculo();
    }

    @Override
    protected MetodosGenericos<EstadoVehiculo> getFacadeLocal() {
        return estadoVehiculoFacade_;
        
        }

    @Override
    protected EstadoVehiculo getEntity() {
        return estadoVehiculo_;
    }
    
     @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        estadoVehiculo_ = (EstadoVehiculo) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        estadoVehiculo_ = new EstadoVehiculo();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }
    
    //********* Getters y Setters *****

    public EstadosCRUD getEstado() {
        return estado;
    }

    public EstadoVehiculo getEstadoVehiculo_() {
        return estadoVehiculo_;
    }

    public void setEstadoVehiculo_(EstadoVehiculo estadoVehiculo_) {
        this.estadoVehiculo_ = estadoVehiculo_;
    }

    public List<EstadoVehiculo> getLista() {
        return lista;
    }

    public void setLista(List<EstadoVehiculo> lista) {
        this.lista = lista;
    }

    public LazyDataModel<EstadoVehiculo> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<EstadoVehiculo> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
}
