/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.ModeloFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.VehiculoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Modelo;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Vehiculo;

/**
 *
 * @author kevin
 */
@Named(value = "frmVehiculo")
@ViewScoped
public class VehiculoBean extends BackingBean<Vehiculo> implements Serializable {

    @EJB
    VehiculoFacadeLocal vehiculoFacade_;
    Vehiculo vehiculo_;
    private EstadosCRUD estado;

    @EJB
    ModeloFacadeLocal modelo;
    private List<Modelo> listaModelo;
            
    public List<Modelo> llenarModelo(){
       
          if (modelo.findAll() != null) {
            return listaModelo = modelo.findAll();
        } else {
            return listaModelo = Collections.EMPTY_LIST;
        }
        
    }

    public List<Modelo> getListaModelo() {
        return listaModelo;
    }
            
    
     @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        vehiculo_= new Vehiculo();
        llenarModelo();
        inicializar();
    }

    @Override
    protected Vehiculo getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Vehiculo item : (List<Vehiculo>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdVehiculo().compareTo(registro.longValue()) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }

    @Override
    protected Object getKey(Vehiculo entity) {
        return entity.getIdVehiculo();
    }

    @Override
    protected MetodosGenericos<Vehiculo> getFacadeLocal() {
        return vehiculoFacade_;
    }

    @Override
    protected Vehiculo getEntity() {
        return vehiculo_;
    }

           @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado=EstadosCRUD.EDITAR;
        vehiculo_ = (Vehiculo) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        vehiculo_ = new Vehiculo();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    
    //**** getters y Setters !°!
    
    
    public EstadosCRUD getEstado() {
        return estado;
    }

    public Vehiculo getVehiculo_() {
        return vehiculo_;
    }

    public void setVehiculo_(Vehiculo vehiculo_) {
        this.vehiculo_ = vehiculo_;
    }

    public List<Vehiculo> getLista() {
        return lista;
    }

    public void setLista(List<Vehiculo> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Vehiculo> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Vehiculo> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
}
