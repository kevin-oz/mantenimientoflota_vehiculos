/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MarcaFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.Marca;

/**
 *
 * @author kevin
 */
@Named(value = "frmMarca")
@ViewScoped
public class MarcaBean extends BackingBean<Marca> implements Serializable {

    @EJB
    MarcaFacadeLocal marcafacade_;
    Marca marca_;
    private EstadosCRUD estado;

    //**** Manejo de imagenes que se desplazan en el inicio
    List<String> images;

    public void suffleImages() {
        images = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            images.add("imagen" + i + ".jpg");
        }
    }

    public List<String> getImages() {
        return images;
    }

    @PostConstruct
    public void init() {
        suffleImages();
        estado = EstadosCRUD.NONE;
        marca_ = new Marca();
        inicializar();
    }

    /*
    metodos sobre-cargados
     */
    @Override
    protected Marca getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (Marca item : (List<Marca>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdMarca().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }

    @Override
    protected Object getKey(Marca entity) {
        return entity.getIdMarca();
    }

    @Override
    protected MetodosGenericos<Marca> getFacadeLocal() {
        return marcafacade_;
    }

    @Override
    protected Marca getEntity() {
        return marca_;
    }

    @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        btncancelarHandler();
        estado = EstadosCRUD.EDITAR;
        marca_ = (Marca) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        marca_ = new Marca();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }

    /*
    getters y setters
     */
    public EstadosCRUD getEstado() {
        return estado;
    }

    public Marca getMarca_() {
        return marca_;
    }

    public void setMarca_(Marca marca_) {
        this.marca_ = marca_;
    }

    public List<Marca> getLista() {
        return lista;
    }

    public void setLista(List<Marca> lista) {
        this.lista = lista;
    }

    public LazyDataModel<Marca> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<Marca> lazyModel) {
        this.lazyModel = lazyModel;
    }


}
