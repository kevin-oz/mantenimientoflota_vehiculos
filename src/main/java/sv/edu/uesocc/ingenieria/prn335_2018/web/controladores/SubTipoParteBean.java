/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.SubTipoParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoParteFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.SubTipoParte;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoParte;

/**
 *
 * @author kevin Figueroa
 */
@Named(value = "frmSubTipoParte")
@ViewScoped
public class SubTipoParteBean extends BackingBean<SubTipoParte> implements Serializable {

    @EJB
    SubTipoParteFacadeLocal subTipoParteFacade_;
    SubTipoParte subTipoParte_;
    private EstadosCRUD estado;
    
    @EJB
    TipoParteFacadeLocal tipoParte;
    private List<TipoParte> listaTipoParte;
    
    public List<TipoParte> llenarLista(){
        if(tipoParte.findAll()!=null){
            return listaTipoParte=tipoParte.findAll();
        }else
        {
            return listaTipoParte=Collections.EMPTY_LIST;
        }
    }

    public List<TipoParte> getListaTipoParte() {
        return listaTipoParte;
    }
    

    
    @PostConstruct
    public void init(){
        estado = EstadosCRUD.NONE;
        subTipoParte_= new SubTipoParte();
        llenarLista();
        inicializar();
    }
    
    //**** sobreCarga de Metodos  ***********
    
    
//    @Override
//    protected Integer getRowD(SubTipoParte object) {
//        return object.getIdSubTipoParte();
//    }
    
    
    @Override
    protected SubTipoParte getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (SubTipoParte item : (List<SubTipoParte>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdSubTipoParte().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }
    

    @Override
    protected Object getKey(SubTipoParte entity) {
        return entity.getIdSubTipoParte();
    }

    @Override
    protected MetodosGenericos<SubTipoParte> getFacadeLocal() {
        return subTipoParteFacade_;
    }

    @Override
    protected SubTipoParte getEntity() {
        return subTipoParte_;
    }
    
           @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado=EstadosCRUD.EDITAR;
        subTipoParte_ = (SubTipoParte) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        subTipoParte_ = new SubTipoParte();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }
    
    
    // ********** getters y setters ***********

    public EstadosCRUD getEstado() {
        return estado;
    }
    
    public SubTipoParte getSubTipoParte_() {
        return subTipoParte_;
    }

    public void setSubTipoParte_(SubTipoParte subTipoParte_) {
        this.subTipoParte_ = subTipoParte_;
    }

    public List<SubTipoParte> getLista() {
        return lista;
    }

    public void setLista(List<SubTipoParte> lista) {
        this.lista = lista;
    }

    public LazyDataModel<SubTipoParte> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<SubTipoParte> lazyModel) {
        this.lazyModel = lazyModel;
    }

    
    
    
    
    

}
