/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.web.controladores;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.LazyDataModel;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.MetodosGenericos;
import sv.edu.uesocc.ingenieria.prn335_2018.datos.acceso.TipoEstadoVehiculoFacadeLocal;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoEstadoVehiculo;

/**
 *
 * @author kevin
 */
@Named(value = "frmTipoEstadoVehiculo")
@ViewScoped
public class TipoEstadoVehiculoBean extends BackingBean<TipoEstadoVehiculo> implements Serializable {

    @EJB
    TipoEstadoVehiculoFacadeLocal tipoestadoVehiculofacade_;
    TipoEstadoVehiculo tipoEstadoVehiculo_;
    private EstadosCRUD estado;

    
      @PostConstruct
    public void init() {
        estado = EstadosCRUD.NONE;
        tipoEstadoVehiculo_ = new TipoEstadoVehiculo();
        inicializar();
    }
    
    
    /*
    sobrecargando los métodos del BeanPrincipal
    */
    
//    @Override
//    protected Integer getRowD(TipoEstadoVehiculo object) {
//        return object.getIdTipoEstadoVehiculo();
//    }
    
    
    @Override
    protected TipoEstadoVehiculo getrowD(String rowkey) {

        if (rowkey != null && !rowkey.isEmpty() && this.getLazyModel().getWrappedData() != null) {
            try {

                for (TipoEstadoVehiculo item : (List<TipoEstadoVehiculo>) this.getLazyModel().getWrappedData()) {
                    Integer registro = new Integer(rowkey);
                    if (item.getIdTipoEstadoVehiculo().compareTo(registro) == 0) {
                        return item;
                    }

                }

            } catch (NumberFormatException e) {
                System.out.println("Excepcion" + e.getMessage());
            }
        }

        return null;

    }

    @Override
    protected Object getKey(TipoEstadoVehiculo entity) {
        return entity.getIdTipoEstadoVehiculo();
    }

    @Override
    protected MetodosGenericos<TipoEstadoVehiculo> getFacadeLocal() {
        return tipoestadoVehiculofacade_;
    }

    @Override
    protected TipoEstadoVehiculo getEntity() {
        return tipoEstadoVehiculo_;
    }

      @Override
    public void crear() {
        estado = EstadosCRUD.AGREGAR;
        super.crear();
        btncancelarHandler();
    }

    @Override
    public void modificar() {
        estado = EstadosCRUD.EDITAR;
        super.modificar();
        btncancelarHandler();
    }

    @Override
    public void eliminar() {
        estado = EstadosCRUD.ELIMINAR;
        super.eliminar();
        btncancelarHandler();

    }

    /**
     *
     * @param event para el seleccionamiento de los campos en la tabla
     */
    public void onRowSelect(SelectEvent event) {
        estado = EstadosCRUD.EDITAR;
        tipoEstadoVehiculo_ = (TipoEstadoVehiculo) event.getObject();
    }

    public void btncancelarHandler() {
        estado = EstadosCRUD.NONE;
        tipoEstadoVehiculo_ = new TipoEstadoVehiculo();
    }

    public void btnNuevoHandler() {
        estado = EstadosCRUD.NUEVO;
    }
    
    
    /*
    getters y setters !!
    */
    
    
    public EstadosCRUD getEstado() {
        return estado;
    }
    

    public TipoEstadoVehiculo getTipoEstadoVehiculo_() {
        return tipoEstadoVehiculo_;
    }

    public void setTipoEstadoVehiculo_(TipoEstadoVehiculo tipoEstadoVehiculo_) {
        this.tipoEstadoVehiculo_ = tipoEstadoVehiculo_;
    }

    public List<TipoEstadoVehiculo> getLista() {
        return lista;
    }

    public void setLista(List<TipoEstadoVehiculo> lista) {
        this.lista = lista;
    }

    public LazyDataModel<TipoEstadoVehiculo> getLazyModel() {
        return lazyModel;
    }

    public void setLazyModel(LazyDataModel<TipoEstadoVehiculo> lazyModel) {
        this.lazyModel = lazyModel;
    }
    
    
    
    
    
    
    
    
    
    
}
